using System.Collections.Generic;
using System.IO;
using System.Text;
using CoreService.Models;
using FluentAssertions;
using GUI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GUITest
{
    [TestClass]
    public class ViewModelTests
    {
        [TestMethod]
        public void TestSaveLoadSequence()
        {
            string json = "{ \"contexts\":[{\"contextName\":\"RECORDING_START\",\"endTime\":\"03.03.2020 17:57:21\",\"events\":[{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5},{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5},{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5}],\"numberOfEvents\":3,\"screenshot\":\"NONE\",\"startTime\":\"03.03.2020 17:57:19\"}],\"endTime\":\"03.03.2020 17:57:21\",\"numberOfContexts\":1,\"startTime\":\"03.03.2020 17:57:19\"}";

            Sequence test = JsonConvert.DeserializeObject<Sequence>(json);

            ViewModel.currentSequence = test;
            ViewModel.currentSeqPath = ViewModel.seqPath + "\\TESTING.seq";
            if (File.Exists(ViewModel.currentSeqPath))
                File.Delete(ViewModel.currentSeqPath);
            ViewModel.SaveSequence(true);

            Assert.IsTrue(File.Exists(ViewModel.currentSeqPath));

            ViewModel.currentSequence = null;

            ViewModel.LoadSequence(ViewModel.currentSeqPath);
            test.Should().BeEquivalentTo(ViewModel.currentSequence);
            if (File.Exists(ViewModel.currentSeqPath))
                File.Delete(ViewModel.currentSeqPath);
        }

        [TestMethod]
        public void TestSaveLoadSettings()
        {
            SettingsModel testSettings = new SettingsModel
            {
                blacklist = false,
                websiteList = new List<string>(),
                screenshotQuality = 20,
                startChrome = true
            };
            ViewModel.settings = new SettingsModel
            {
                blacklist = false,
                websiteList = new List<string>(),
                screenshotQuality = 20,
                startChrome = true
            };
            ViewModel.SaveSettings();
            ViewModel.settings.blacklist = true;
            ViewModel.LoadSettings();
            testSettings.Should().BeEquivalentTo(ViewModel.settings);
        }

        [TestMethod]
        public void TestLoadSettings2()
        {
            SettingsModel testSettings = new SettingsModel
            {
                blacklist = true,
                websiteList = new List<string>(),
                screenshotQuality = 50,
                startChrome = true
            };
            if (File.Exists(ViewModel.settingsFilePath))
                File.Delete(ViewModel.settingsFilePath);
            ViewModel.LoadSettings();
            testSettings.Should().BeEquivalentTo(ViewModel.settings);
        }

        [TestMethod]
        public void TestDeleteEvent()
        {
            string json = "{ \"contexts\":[{\"contextName\":\"RECORDING_START\",\"endTime\":\"03.03.2020 17:57:21\",\"events\":[{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5},{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5},{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5}],\"numberOfEvents\":3,\"screenshot\":\"NONE\",\"startTime\":\"03.03.2020 17:57:19\"}],\"endTime\":\"03.03.2020 17:57:21\",\"numberOfContexts\":1,\"startTime\":\"03.03.2020 17:57:19\"}";
            string json2 = "{ \"contexts\":[{\"contextName\":\"RECORDING_START\",\"endTime\":\"03.03.2020 17:57:21\",\"events\":[{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5},{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5}],\"numberOfEvents\":2,\"screenshot\":\"NONE\",\"startTime\":\"03.03.2020 17:57:19\"}],\"endTime\":\"03.03.2020 17:57:21\",\"numberOfContexts\":1,\"startTime\":\"03.03.2020 17:57:19\"}";

            Sequence test = JsonConvert.DeserializeObject<Sequence>(json);
            Sequence test2 = JsonConvert.DeserializeObject<Sequence>(json2);

            ViewModel.currentSequence = test;
            ViewModel.selected = Selectables.Event;
            ViewModel.currentEvent = 0;
            ViewModel.currentContext = 0;

            ViewModel.DeleteSelected(new AnalyseWindow());

            ViewModel.currentSequence.Should().BeEquivalentTo(test2);
        }

        [TestMethod]
        public void TestDeleteContext()
        {
            string json = "{ \"contexts\":[{\"contextName\":\"RECORDING_START\",\"endTime\":\"03.03.2020 17:57:21\",\"events\":[{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5},{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5},{\"__type\":\"MouseEvent:#CoreService.Models.Events\",\"eventID\":3,\"name\":\"Maus-Event\",\"screenshot\":\"NONE\",\"time\":\"19:14:2\",\"clicktype\":\"Left\",\"xCoordinate\":5,\"yCoordinate\":5}],\"numberOfEvents\":3,\"screenshot\":\"NONE\",\"startTime\":\"03.03.2020 17:57:19\"}],\"endTime\":\"03.03.2020 17:57:21\",\"numberOfContexts\":1,\"startTime\":\"03.03.2020 17:57:19\"}";
            string json2 = "{ \"contexts\":[],\"endTime\":\"03.03.2020 17:57:21\",\"numberOfContexts\":0,\"startTime\":\"03.03.2020 17:57:19\"}";

            Sequence test = JsonConvert.DeserializeObject<Sequence>(json);
            Sequence test2 = JsonConvert.DeserializeObject<Sequence>(json2);

            ViewModel.currentSequence = test;
            ViewModel.selected = Selectables.Context;
            ViewModel.currentContext = 0;

            ViewModel.DeleteSelected(new AnalyseWindow());

            ViewModel.currentSequence.Should().BeEquivalentTo(test2);
        }
    }
}
