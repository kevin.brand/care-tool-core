using System;
using System.IO.Pipes;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using CoreService.Models;
using CoreService.Models.Events;
using FluentAssertions;
using GUI;
using Microsoft.Owin.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace EventReceiverTest
{
    [TestClass]
    public class EventReceiverTests
    {
        private string _baseAddress;
        private EventModel testEventModel;
        public EventReceiverTests()
        {
            _baseAddress = "http://localhost:15578/";
            WebApp.Start<EventReceiver.Startup>(url: _baseAddress);
            NamedPipeServerStream server = new NamedPipeServerStream("eventPipe", PipeDirection.In, 1, PipeTransmissionMode.Byte, PipeOptions.Asynchronous);
            server.BeginWaitForConnection(WaitForConnectionCallBack, server);
        }


        [TestMethod]
        public void EventReceiver_started()
        {
            bool responded;
            using (var client = new TcpClient())
            {
                var result = client.BeginConnect("127.0.0.1", 15578, null, null);
                var success = result.AsyncWaitHandle.WaitOne(3000);
                client.EndConnect(result);
                responded = success;
            }
            Assert.IsTrue(responded, "EventReceiver does not respond");
        }

        [TestMethod]
        public void Test_GetConfig()
        {
            bool success;
            string response;
            using (HttpClient client = new HttpClient())
            {
                response = client.GetStringAsync("http://localhost:15578/api/getConfig").Result;
            }

            try
            {
                JsonConvert.DeserializeObject<SettingsModel>(response);
                success = true;
            }
            catch (Exception e)
            {
                success = false;
            }
            Assert.IsTrue(success, "Returned config does not parse to a SettingsModel");
        }

        [TestMethod]
        public void Test_PostEvent()
        {
            testEventModel = null;
            string json = "{ \"eventID\": \"1\", \"newContext\": \"https://www.google.de\", \"description\": \"Website Loaded\", \"screenshot\": \"NONE\", \"time\": \"19:14:2\"}";
            EventModel test = new ContextSwitch()
            {
                description = "Website Loaded",
                eventID = 1,
                screenshot = "NONE",
                time = "19:14:2",
                newContext = "https://www.google.de"
            };
            using (HttpClient client = new HttpClient())
            {
                var response = client.PostAsync("http://localhost:15578/api/postEvent/1", new StringContent(json, Encoding.UTF8, "application/json")).Result;
            }
            Thread.Sleep(1000);
            Assert.IsNotNull(testEventModel);
            test.Should().BeEquivalentTo(testEventModel);
        }

        [TestMethod]
        public void Test_PostExcelEvent()
        {
            testEventModel = null;
            string json = "{ \"eventID\": \"1\", \"newContext\": \"Worksheet 1\", \"description\": \"Worksheet switched\", \"screenshot\": \"NONE\", \"time\": \"19:14:2\"}";
            EventModel test = new ContextSwitch()
            {
                description = "Worksheet switched",
                eventID = 1,
                screenshot = "NONE",
                time = "19:14:2",
                newContext = "Worksheet 1"
            };
            using (HttpClient client = new HttpClient())
            {
                var response = client.PostAsync("http://localhost:15578/api/postEvent/excel/1", new StringContent(json, Encoding.UTF8, "application/json")).Result;
            }
            Thread.Sleep(1000);
            Assert.IsNotNull(testEventModel);
            Assert.AreEqual(test.name, testEventModel.name);
            Assert.AreEqual(test.time, testEventModel.time);
            Assert.AreEqual(test.eventID, testEventModel.eventID);
        }

        private void createEvent(string data)
        {
            int idbuff = 0;
            for (int i = data.Length - 1; i >= 0; i--)
            {
                if (data[i] == '}')
                    break;
                idbuff++;
            }
            int id = Convert.ToInt32(data.Substring(data.Length - idbuff));
            string json = data.Remove(data.Length - idbuff);
            dynamic newEvent = JsonConvert.DeserializeObject(json, EventDictionary.eventDictionary[id]);
            testEventModel = newEvent;
        }

        private void WaitForConnectionCallBack(IAsyncResult iar)
        {
            try
            {
                NamedPipeServerStream server = (NamedPipeServerStream)iar.AsyncState;
                server.EndWaitForConnection(iar);

                byte[] sizebuff = new byte[4];
                server.Read(sizebuff, 0, 4);
                int size = BitConverter.ToInt32(sizebuff, 0);
                byte[] buffer = new byte[size];

                server.Read(buffer, 0, size);
                string stringData = Encoding.UTF8.GetString(buffer, 0, buffer.Length);

                createEvent(stringData);

                server.Close();
                server = new NamedPipeServerStream("eventPipe", PipeDirection.In, 1, PipeTransmissionMode.Byte, PipeOptions.Asynchronous);

                server.BeginWaitForConnection(WaitForConnectionCallBack, server);
            }
            catch
            {
            }
        }
    }
}
