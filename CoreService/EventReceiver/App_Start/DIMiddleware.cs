﻿using Autofac;
using Autofac.Integration.WebApi;
using EventReceiver.Services;
using EventReceiver.Services.Interfaces;
using Hangfire;
using Owin;
using System.Reflection;
using System.Web.Http;


namespace EventReceiver.App_Start
{
    public static class ContainerProvider
    {
        public static IContainer Container { get; set; }
    }

    public static class DIMiddleware
    {
        public static void ConfigureDIMiddleware(this IAppBuilder app, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<EventReceiverService>().As<IEventReceiverService>().InstancePerDependency();

            var container = builder.Build();

            var webApiResolver = new AutofacWebApiDependencyResolver(container);
            config.DependencyResolver = webApiResolver;

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);

            GlobalConfiguration.Configuration.UseAutofacActivator(container);

            ContainerProvider.Container = container;
        }

    }
}