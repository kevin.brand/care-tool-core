﻿using EventReceiver.App_Start;
using Owin;
using System.Web.Http;

namespace EventReceiver
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            WebApiConfig.Register(config);
            app.ConfigureDIMiddleware(config);
            app.ConfigureCorsPolicy();
            app.UseWebApi(config);

            config.EnsureInitialized();
        }
    }
}
