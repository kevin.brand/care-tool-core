﻿using EventReceiver.Services.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Encoder = System.Drawing.Imaging.Encoder;

namespace EventReceiver.Services
{
    public class EventReceiverService : IEventReceiverService
    {
        private string settingsPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CaReTrackingTool\\settings.json";

        public void ReceiveEvent(JObject _event, int id)
        {
            Console.WriteLine("Event received");
            NamedPipeClientStream client = new NamedPipeClientStream(".", "eventPipe", PipeDirection.Out, PipeOptions.Asynchronous);
            client.Connect();

            byte[] _buffer = Encoding.UTF8.GetBytes(_event.ToString() + id.ToString());
            int size = _buffer.Length;
            byte[] sizebuff = new byte[4];
            sizebuff = BitConverter.GetBytes(size);
            _buffer = sizebuff.Concat(_buffer).ToArray();
            client.BeginWrite(_buffer, 0, _buffer.Length, AsyncSend, client);
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct Rect
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        public void ReceiveExcelEvent(JObject _event, int id)
        {
            Console.WriteLine("Excel Event received");

            string image;
            Rect rect = new Rect();
            try
            {
                GetWindowRect(GetForegroundWindow(), ref rect);
                Rectangle bounds = new Rectangle(rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top);
                Bitmap result = new Bitmap(bounds.Width, bounds.Height);

                using (Graphics graphics = Graphics.FromImage(result))
                {
                    graphics.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
                }

                int compressionLevel = (int)getConfig()["screenshotQuality"];

                ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                Encoder encoder = Encoder.Quality;
                EncoderParameters encParams = new EncoderParameters(1);
                EncoderParameter encParam = new EncoderParameter(encoder, compressionLevel);
                encParams.Param[0] = encParam;

                MemoryStream ms = new MemoryStream();
                result.Save(ms, jpgEncoder, encParams);

                image = Convert.ToBase64String(ms.GetBuffer());
            }
            catch
            {
                image = "NONE";
            }

            if ((int)_event["eventID"] == 3)
            {
                _event["xCoordinate"] = Cursor.Position.X - rect.Left;
                _event["yCoordinate"] = Cursor.Position.Y - rect.Top;
            }
            _event["screenshot"] = image;

            NamedPipeClientStream client = new NamedPipeClientStream(".", "eventPipe", PipeDirection.Out, PipeOptions.Asynchronous);
            client.Connect();

            byte[] _buffer = Encoding.UTF8.GetBytes(_event.ToString() + id.ToString());
            int size = _buffer.Length;
            byte[] sizebuff = new byte[4];
            sizebuff = BitConverter.GetBytes(size);
            _buffer = sizebuff.Concat(_buffer).ToArray();
            client.BeginWrite(_buffer, 0, _buffer.Length, AsyncSend, client);
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
                if (codec.FormatID == format.Guid)
                    return codec;

            return null;
        }

        private void AsyncSend(IAsyncResult iar)
        {
            try
            {
                NamedPipeClientStream client = (NamedPipeClientStream)iar.AsyncState;

                client.EndWrite(iar);
                client.Flush();
                client.Close();
                client.Dispose();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        public JObject getConfig() => JObject.Parse(File.ReadAllText(settingsPath));
    }
}