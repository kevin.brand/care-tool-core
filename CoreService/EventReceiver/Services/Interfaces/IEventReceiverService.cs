﻿using Newtonsoft.Json.Linq;

namespace EventReceiver.Services.Interfaces
{
    public interface IEventReceiverService
    {
        void ReceiveEvent(JObject _event, int id);
        void ReceiveExcelEvent(JObject _event, int id);
        JObject getConfig();
    }
}