﻿using EventReceiver.Services.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Http;

namespace EventReceiver.Controller
{
    /// <summary>
    /// Defines the routes, that the recorders can call
    /// </summary>
    [RoutePrefix("api")]
    public class EventController : ApiController
    {
        public IEventReceiverService _eventReceiverService;

        public EventController(IEventReceiverService eventReceiverService)
        {
            _eventReceiverService = eventReceiverService ?? throw new ArgumentNullException(nameof(eventReceiverService));
        }

        [HttpPost]
        [Route("postEvent/{id}")]
        public void postEvent([FromBody] JObject _event, [FromUri] int id) => _eventReceiverService.ReceiveEvent(_event, id);

        [HttpPost]
        [Route("postEvent/excel/{id}")]
        public void postExcelEvent([FromBody] JObject _event, [FromUri] int id) => _eventReceiverService.ReceiveExcelEvent(_event, id);

        [HttpGet]
        [Route("getConfig")]
        public JObject getConfig() => _eventReceiverService.getConfig();
    }
}