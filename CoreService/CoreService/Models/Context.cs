﻿using CoreService.Models.Events;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CoreService.Models
{
    /// <summary>
    /// Data model for contexts
    /// </summary>
    [DataContract]
    public class Context
    {
        [DataMember]
        public string startTime { get; private set; }
        [DataMember]
        public string endTime { get; set; }
        [DataMember]
        public int numberOfEvents { get; set; }
        [DataMember]
        public string contextName { get; private set; }
        [DataMember]
        public string screenshot { get; private set; }
        [DataMember]
        public List<EventModel> events { get; set; }

        public Context(string startTime, string screenshot, string contextName)
        {
            this.startTime = startTime;
            this.numberOfEvents = 0;      
            this.contextName = contextName;
            this.screenshot = screenshot;
            events = new List<EventModel>();
        }
    }
}
