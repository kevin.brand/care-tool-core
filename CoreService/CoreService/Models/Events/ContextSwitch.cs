﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class ContextSwitch : EventModel
    {
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string newContext { get; set; }
        public ContextSwitch()
        {
            name = "Kontextwechsel";
        }
    }
}
