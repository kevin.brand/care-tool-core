﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class TextSelectEvent : EventModel
    {
        [DataMember]
        public string text { get; set; }

        public TextSelectEvent()
        {
            name = "Textauswahl-Event";
        }
    }
}
