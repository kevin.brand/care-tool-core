﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class CollectionEvent : EventModel
    {
        [DataMember]
        public string obj { get; set; }

        public CollectionEvent()
        {
            name = "Collection Event";
        }
    }
}
