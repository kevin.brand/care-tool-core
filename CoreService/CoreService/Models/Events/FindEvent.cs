﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class FindEvent : EventModel
    {
        public FindEvent()
        {
            name = "Such-Event";
        }
    }
}
