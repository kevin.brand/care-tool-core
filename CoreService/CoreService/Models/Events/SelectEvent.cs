﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class SelectEvent : EventModel
    {
        [DataMember]
        public string data { get; set; }

        public SelectEvent()
        {
            name = "Select-Event";
        }
    }
}
