﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class SortEvent : EventModel
    {
        [DataMember]
        public string range { get; set; }
        [DataMember]
        public string type { get; set; }

        public SortEvent()
        {
            name = "Sortier-Event";
        }
    }
}
