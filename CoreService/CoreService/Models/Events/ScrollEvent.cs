﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class ScrollEvent : EventModel
    {
        [DataMember]
        public float xStartCoordinate { get; set; }
        [DataMember]
        public float xEndCoordinate { get; set; }
        [DataMember]
        public float yStartCoordinate { get; set; }
        [DataMember]
        public float yEndCoordinate { get; set; }

        public ScrollEvent()
        {
            name = "Scroll-Event";
        }
    }
}
