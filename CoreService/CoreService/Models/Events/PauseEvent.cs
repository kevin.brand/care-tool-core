﻿using System;
using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class PauseEvent : EventModel
    {
        [DataMember]
        public string endTime { get; set; }

        public PauseEvent()
        {
            time = DateTime.Now.ToString();
            eventID = 4;
            name = "Pause-Event";
            screenshot = "NONE";
        }
    }
}
