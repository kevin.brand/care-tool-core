﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class FormSubmitEvent : EventModel
    {
        [DataMember]
        public string data { get; set; }

        public FormSubmitEvent()
        {
            name = "Form-Submit-Event";
        }
    }
}
