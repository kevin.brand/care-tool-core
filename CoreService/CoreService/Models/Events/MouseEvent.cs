﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class MouseEvent : EventModel
    {
        [DataMember]
        public string clicktype { get; set; }
        [DataMember]
        public int xCoordinate { get; set; }
        [DataMember]
        public int yCoordinate { get; set; }

        public MouseEvent()
        {
            name = "Maus-Event";
        }
    }
}
