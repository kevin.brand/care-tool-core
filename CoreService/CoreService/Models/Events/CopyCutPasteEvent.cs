﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class CopyCutPasteEvent : EventModel
    {
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string text { get; set; }

        public CopyCutPasteEvent()
        {
            name = "CopyCutPaste-Event";
        }
    }
}
