﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract,
        KnownType(typeof(CollectionEvent)),
        KnownType(typeof(ContextSwitch)),
        KnownType(typeof(CopyCutPasteEvent)),
        KnownType(typeof(FindEvent)),
        KnownType(typeof(FormSubmitEvent)),
        KnownType(typeof(MouseEvent)),
        KnownType(typeof(PauseEvent)),
        KnownType(typeof(ScrollEvent)),
        KnownType(typeof(SelectEvent)),
        KnownType(typeof(SortEvent)),
        KnownType(typeof(TextInputEvent)),
        KnownType(typeof(TextSelectEvent))]
    public class EventModel
    {
        [DataMember]
        public int eventID { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string screenshot { get; set; }
        [DataMember]
        public string time { get; set; }

        public EventModel()
        {
            name = "";
        }
    }
}
