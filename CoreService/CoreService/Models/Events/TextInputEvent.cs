﻿using System.Runtime.Serialization;

namespace CoreService.Models.Events
{
    [DataContract]
    public class TextInputEvent : EventModel
    {
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public string dom { get; set; }

        public TextInputEvent()
        {
            name = "Textinput-Event";
        }
    }
}
