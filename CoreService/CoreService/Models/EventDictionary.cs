﻿using CoreService.Models.Events;
using System;
using System.Collections.Generic;

namespace CoreService.Models
{
    /// <summary>
    /// Maps all event types to an ID
    /// </summary>
    public static class EventDictionary
    {
        public static Dictionary<int, Type> eventDictionary = new Dictionary<int, Type>()
        {
            { 0, new CollectionEvent().GetType() },
            { 1, new ContextSwitch().GetType() },
            { 2, new FormSubmitEvent().GetType() },
            { 3, new MouseEvent().GetType() },
            { 4, new PauseEvent().GetType() },
            { 5, new ScrollEvent().GetType() },
            { 6, new SelectEvent().GetType() },
            { 7, new CopyCutPasteEvent().GetType() },
            { 8, new SortEvent().GetType() },
            { 9, new TextInputEvent().GetType() },
            { 10, new TextSelectEvent().GetType() },
            { 11, new FindEvent().GetType() }
        };
    }
}
