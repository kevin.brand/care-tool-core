﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CoreService.Models
{
    /// <summary>
    /// Data model for sequences
    /// </summary>
    [DataContract]
    public class Sequence
    {
        [DataMember]
        public string startTime { get; private set; }
        [DataMember]
        public string endTime { get; set; }
        [DataMember]
        public int numberOfContexts { get; set; }
        [DataMember]
        public List<Context> contexts { get; set; }

        public Sequence()
        {
            startTime = DateTime.Now.ToString();
            numberOfContexts = 0;
            contexts = new List<Context>();
        }
    }
}
