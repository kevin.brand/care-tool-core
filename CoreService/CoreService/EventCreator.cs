﻿using System;
using System.IO.Pipes;
using System.Text;
using CoreService.Models;
using CoreService.Models.Events;

namespace CoreService
{
    /// <summary>
    /// Receives events from the EventReceiver and parses them into data models
    /// </summary>
    public class EventCreator
    {
        public delegate void EventModelCreatedEventHandler(EventModel eventModel);

        public event EventModelCreatedEventHandler eventcreated;

        public EventCreator()
        {
            NamedPipeServerStream server = new NamedPipeServerStream("eventPipe", PipeDirection.In, 1, PipeTransmissionMode.Byte, PipeOptions.Asynchronous);
            server.BeginWaitForConnection(WaitForConnectionCallBack, server);
        }

        private void createEvent(string data)
        {
            int idbuff = 0;
            for (int i = data.Length - 1; i >= 0; i--)
            {
                if (data[i] == '}')
                    break;
                idbuff++;
            }
            int id = Convert.ToInt32(data.Substring(data.Length - idbuff));
            string json = data.Remove(data.Length - idbuff);
            dynamic newEvent = Newtonsoft.Json.JsonConvert.DeserializeObject(json, EventDictionary.eventDictionary[id]);
            OnEventCreated(newEvent);
        }

        private void OnEventCreated(EventModel eventmodel) => eventcreated?.Invoke(eventmodel);

        private void WaitForConnectionCallBack(IAsyncResult iar)
        {
            try
            {
                NamedPipeServerStream server = (NamedPipeServerStream)iar.AsyncState;
                server.EndWaitForConnection(iar);

                byte[] sizebuff = new byte[4];
                server.Read(sizebuff, 0, 4);
                int size = BitConverter.ToInt32(sizebuff, 0);
                byte[] buffer = new byte[size];

                server.Read(buffer, 0, size);
                string stringData = Encoding.UTF8.GetString(buffer, 0, buffer.Length);

                createEvent(stringData);

                server.Close();
                server = new NamedPipeServerStream("eventPipe", PipeDirection.In, 1, PipeTransmissionMode.Byte, PipeOptions.Asynchronous);

                server.BeginWaitForConnection(WaitForConnectionCallBack, server);
            }
            catch
            {
            }
        }
    }
}
