﻿using CoreService.Models;
using CoreService.Models.Events;
using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using LiquidEngine.Tools;

namespace CoreService
{
    /// <summary>
    /// The SequenceController is in charge of managing the data structure of the sequence which is being recorded.
    /// </summary>
    public class SequenceController
    {
        string seqPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Documents\Sequenzen";

        public Sequence currentSequence;
        public Context currentContext;
        bool checkPause;
        PauseEvent pauseEvent;

        public EventCreator eventCreator;

        public SequenceController()
        {
            eventCreator = new EventCreator();

            if (!Directory.Exists(seqPath))
                Directory.CreateDirectory(seqPath);
        }

        public void Start()
        {
            currentSequence = new Sequence();
            currentContext = new Context(DateTime.Now.ToString(), "NONE", "RECORDING_START");
            currentSequence.contexts.Add(currentContext);
            currentSequence.numberOfContexts++;

            eventCreator.eventcreated += AddEvent;
        }
        
        public void Stop()
        {
            eventCreator.eventcreated -= AddEvent;

            SaveSequence();
        }

        public void Pause()
        {
            if (!checkPause)
            {
                pauseEvent = new PauseEvent();
                checkPause = true;
            }
            else
            {
                pauseEvent.endTime = DateTime.Now.ToString();
                checkPause = false;
                AddEvent(pauseEvent);
            }
        }

        private void SaveSequence()
        {
            currentContext.endTime = DateTime.Now.ToString();
            currentSequence.endTime = DateTime.Now.ToString();

            MemoryTributary stream = new MemoryTributary();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Sequence));
            ser.WriteObject(stream, currentSequence);
            stream.Position = 0;
            StreamReader sr = new StreamReader(stream);

            if (!Directory.Exists(seqPath))
                Directory.CreateDirectory(seqPath);

            string username = Environment.UserName;
            string hash;
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(username));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                    builder.Append(bytes[i].ToString("x2"));
                hash = builder.ToString();
            }

            File.WriteAllText(seqPath + @"\" + DateTime.Now.ToString().Replace(' ', '_').Replace(':', '_').Replace('.', '_').Replace('/', '_') + "__" + hash + ".seq", sr.ReadToEnd());
            stream.Close();
        }
        
        /// <summary>
        /// Adds an event to the current context in the sequence, creates new context on ContextSwitchEvent
        /// </summary>
        /// <param name="eventmodel"></param>
        private void AddEvent(EventModel eventmodel)
        {
            if (checkPause) return;
            if (eventmodel.GetType() == new ContextSwitch().GetType())
            {
                ContextSwitch contextSwitch = (ContextSwitch)eventmodel;
                Context newContext = new Context(contextSwitch.time, contextSwitch.screenshot, contextSwitch.newContext);

                currentContext.endTime = contextSwitch.time;
                currentContext.events.Add(contextSwitch);
                currentContext.numberOfEvents++;

                currentSequence.contexts.Add(newContext);
                currentSequence.numberOfContexts++;

                currentContext = newContext;
            }
            else
            {
                currentContext.events.Add(eventmodel);
                currentContext.numberOfEvents++;
            }
        }
    }
}