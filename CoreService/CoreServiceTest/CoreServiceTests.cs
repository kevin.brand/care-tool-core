using System;
using System.Diagnostics;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using CoreService;
using CoreService.Models.Events;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;

namespace CoreServiceTest
{
    [TestClass]
    public class CoreServiceTests
    {
        private SequenceController s;
        public CoreServiceTests()
        {
            
        }

        [TestMethod]
        public void TestEventCreator()
        {
            s = new SequenceController();
            s.Start();
            string json = "{ \"eventID\": \"3\", \"clicktype\": \"Left\", \"xCoordinate\": \"5\", \"yCoordinate\": \"5\", \"screenshot\": \"NONE\", \"time\": \"19:14:2\"}";
            JObject _event = JObject.Parse(json);
            EventModel test = new MouseEvent()
            {
                clicktype = "Left",
                eventID = 3,
                screenshot = "NONE",
                time = "19:14:2",
                xCoordinate = 5,
                yCoordinate = 5
            };

            SendEvent(_event, 3);

            Thread.Sleep(1000);

            test.Should().BeEquivalentTo(s.currentContext.events.Last());
        }

        [TestMethod]
        public void TestContextSwitch()
        {
            s = new SequenceController();
            s.Start();

            string json = "{ \"eventID\": \"1\", \"newContext\": \"https://www.google.de\", \"description\": \"Website Loaded\", \"screenshot\": \"NONE\", \"time\": \"19:14:2\"}";
            JObject _event = JObject.Parse(json);
            ContextSwitch test = new ContextSwitch()
            {
                description = "Website Loaded",
                eventID = 1,
                screenshot = "NONE",
                time = "19:14:2",
                newContext = "https://www.google.de"
            };

            SendEvent(_event, 1);

            Thread.Sleep(5000);

            Assert.AreEqual(s.currentContext.contextName, test.newContext);
        }

        [TestMethod]
        public void TestPause()
        {
            s = new SequenceController();
            s.Start();
            s.Pause();

            string json = "{ \"eventID\": \"3\", \"clicktype\": \"Left\", \"xCoordinate\": \"5\", \"yCoordinate\": \"5\", \"screenshot\": \"NONE\", \"time\": \"19:14:2\"}";
            JObject _event = JObject.Parse(json);

            SendEvent(_event, 3);

            Thread.Sleep(1000);

            s.Pause();

            Assert.AreEqual(1, s.currentContext.numberOfEvents);
        }

        [TestMethod]
        public void TestStop()
        {
            s = new SequenceController();
            s.Start();

            string json = "{ \"eventID\": \"3\", \"clicktype\": \"Left\", \"xCoordinate\": \"5\", \"yCoordinate\": \"5\", \"screenshot\": \"NONE\", \"time\": \"19:14:2\"}";
            JObject _event = JObject.Parse(json);
            SendEvent(_event, 3);
            Thread.Sleep(500);
            SendEvent(_event, 3);
            Thread.Sleep(500);
            SendEvent(_event, 3);
            Thread.Sleep(500);
            s.Stop();

            int eventNum = s.currentContext.numberOfEvents;
            Assert.AreEqual(3, eventNum);
            SendEvent(_event, 3);
            Thread.Sleep(500);
            Assert.AreEqual(eventNum, s.currentContext.numberOfEvents);
        }

        private void SendEvent(JObject _event, int id)
        {
            NamedPipeClientStream client = new NamedPipeClientStream(".", "eventPipe", PipeDirection.Out, PipeOptions.Asynchronous);
            client.Connect();

            byte[] _buffer = Encoding.UTF8.GetBytes(_event.ToString() + id.ToString());
            int size = _buffer.Length;
            byte[] sizebuff = new byte[4];
            sizebuff = BitConverter.GetBytes(size);
            _buffer = sizebuff.Concat(_buffer).ToArray();
            client.BeginWrite(_buffer, 0, _buffer.Length, AsyncSend, client);
        }

        private void AsyncSend(IAsyncResult iar)
        {
            try
            {
                NamedPipeClientStream client = (NamedPipeClientStream)iar.AsyncState;

                client.EndWrite(iar);
                client.Flush();
                client.Close();
                client.Dispose();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }
    }
}
