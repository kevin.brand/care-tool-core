﻿using Newtonsoft.Json.Linq;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Script.Serialization;
using Encoder = System.Drawing.Imaging.Encoder;

namespace WindowsRecorder
{
    public class WindowsRecorder
    {
        WinEventDelegate dele;
        private const uint WINEVENT_OUTOFCONTEXT = 0;
        private const uint EVENT_SYSTEM_FOREGROUND = 3;
        private static string settingsPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CaReTrackingTool\\settings.json";

        public WindowsRecorder()
        {
            dele = WinEventProc;
            IntPtr m_hhook = SetWinEventHook(EVENT_SYSTEM_FOREGROUND, EVENT_SYSTEM_FOREGROUND, IntPtr.Zero, dele, 0, 0, WINEVENT_OUTOFCONTEXT);
        }

        [DllImport("user32.dll")]
        static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, WinEventDelegate lpfnWinEventProc, uint idProcess, uint idThread, uint dwFlags);

        delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        [StructLayout(LayoutKind.Sequential)]
        private struct Rect
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);

        private string GetActiveWindowTitle()
        {
            const int nChars = 256;
            StringBuilder Buff = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();

            return GetWindowText(handle, Buff, nChars) > 0 ? Buff.ToString() : null;
        }

        private async void WinEventProc(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime)
        {
            string newProcess = GetActiveWindowTitle();
            string screen = CaptureWindow(GetForegroundWindow());

            RecorderEventTemplate newevent = new RecorderEventTemplate()
            {
                eventID = 1,
                time = DateTime.Now.ToString(),
                screenshot = screen,
                description = "Fokussierter Prozess gewechselt",
                newContext = newProcess
            };

            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(RecorderEventTemplate));
            ser.WriteObject(stream, newevent);
            stream.Position = 0;
            StreamReader sr = new StreamReader(stream);

            string json = sr.ReadToEnd();

            using (HttpClient client = new HttpClient())
            {
                var response = await client.PostAsync("http://localhost:15578/api/postEvent/1", new StringContent(json, Encoding.UTF8, "application/json"));
            }
        }

        private static string CaptureWindow(IntPtr handle)
        {
            try
            {
                var rect = new Rect();
                GetWindowRect(handle, ref rect);
                var bounds = new Rectangle(rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top);
                var result = new Bitmap(bounds.Width, bounds.Height);

                using (var graphics = Graphics.FromImage(result))
                {
                    graphics.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
                }


                JObject settings = JObject.Parse(File.ReadAllText(settingsPath));
                int compressionLevel = (int)settings["screenshotQuality"];

                ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                Encoder encoder = Encoder.Quality;
                EncoderParameters encParams = new EncoderParameters(1);
                EncoderParameter encParam = new EncoderParameter(encoder, compressionLevel);
                encParams.Param[0] = encParam;
                MemoryStream ms = new MemoryStream();

                result.Save(ms, jpgEncoder, encParams);

                return Convert.ToBase64String(ms.GetBuffer());
            }
            catch
            {
                return "NONE";
            }
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
                if (codec.FormatID == format.Guid)
                    return codec;
            return null;
        }
    }

    [DataContract]
    public class RecorderEventTemplate
    {
        [DataMember]
        public int eventID { get; set; }
        [DataMember]
        public string time { get; set; }
        [DataMember]
        public string screenshot { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string newContext { get; set; }
    }
}
