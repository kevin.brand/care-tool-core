﻿using System;
using System.Drawing;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Forms;
using static GUI.ViewModel;
using MessageBox = System.Windows.MessageBox;
using Timer = System.Timers.Timer;

namespace GUI
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private NotifyIcon notifyIcon;
        AnalyseWindow analyseWindow;
        private Timer notifyTimer;

        public MainWindow()
        {
            InitializeComponent();

            Initialize();

            Loaded += Window_Loaded;

            notifyIcon = new NotifyIcon
            {
                BalloonTipTitle = "Aufnahme gestartet.",
                BalloonTipText = " ",
                Visible = true,
                Icon = new Icon(@"Resources\greycircle-32.ico")
            };
            notifyIcon.Click += NotifyIcon_Click;

            notifyTimer = new Timer(2000);
            notifyTimer.AutoReset = true;
            notifyTimer.Start();

            statusChanged += StatusChange;

            Closed += delegate { notifyIcon.Visible = false; if (started) {if (MessageBox.Show("Laufende Aufnahme speichern?", "Aufname speichern", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {Start();}} analyseWindow.Close();};
        }

        private void NotifyIcon_Click(object sender, EventArgs e) => Show();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Rect desktopWorkingArea = SystemParameters.WorkArea;
            Left = desktopWorkingArea.Right - (Width + 50);
            Top = desktopWorkingArea.Bottom - Height;
        }

        private void Btn_analyse_Click(object sender, RoutedEventArgs e)
        {
            analyseWindow = new AnalyseWindow();
            analyseWindow.Show();
            analyseWindow.Closed += AnalyseWindow_Closed;
            analyseWindow.btn_start.Click += delegate { ShowBallonTipStart(); };
            analyseWindow.btn_pause.Click += delegate {  ShowBallonTipPause(); };
            Hide();
        }

        private void ShowBallonTipStart()
        {
            notifyIcon.Icon = started ? new Icon(@"Resources\redcircle-32.ico") : new Icon(@"Resources\greycircle-32.ico");
            notifyIcon.BalloonTipTitle = started ? "Aufnahme gestartet." : "Aufnahme gestoppt.";
            notifyIcon.ShowBalloonTip(500);

            if (started)
                notifyTimer.Elapsed += NotifyIconAnimation;
            else
                notifyTimer.Elapsed -= NotifyIconAnimation;
        }

        private void ShowBallonTipPause()
        {
            notifyIcon.Icon = paused ? new Icon(@"Resources\yellowcircle-32.ico") : new Icon(@"Resources\redcircle-32.ico");
            notifyIcon.BalloonTipTitle = paused ? "Aufnahme pausiert." : "Aufnahme fortgesetzt.";
            notifyIcon.ShowBalloonTip(500);


            if (paused)
                notifyTimer.Elapsed -= NotifyIconAnimation;
            else
                notifyTimer.Elapsed += NotifyIconAnimation;
        }

        private void AnalyseWindow_Closed(object sender, EventArgs e) => Show();

        private void StatusChange(int type)
        {
            if (type == 0)
            {
                btn_startstop.Visibility = paused ? Visibility.Hidden : Visibility.Visible;
                btn_pause.Content = paused ? FindResource("startImg") : FindResource("pauseImg");
            }
            else if (type == 1)
            {
                btn_startstop.Content = started ? FindResource("stopImg") : FindResource("startImg");
                btn_pause.Visibility = (btn_pause.Visibility == Visibility.Visible) ? Visibility.Hidden : Visibility.Visible;
            }
        }

        private void Btn_startstop_Click(object sender, RoutedEventArgs e)
        {
            Start();
            ShowBallonTipStart();
        }

        private void NotifyIconAnimation(object sender, ElapsedEventArgs e)
        {
            notifyIcon.Icon = new Icon(@"Resources\darkredcircle-32.ico");
            Thread.Sleep(200);
            notifyIcon.Icon = new Icon(@"Resources\redcircle-32.ico");
        }

        private void Btn_pause_Click(object sender, RoutedEventArgs e)
        {
            Pause();
            ShowBallonTipPause();
        }

        private void Btn_close_Click(object sender, RoutedEventArgs e) => Close();

        private void Btn_minimize_Click(object sender, RoutedEventArgs e) => Hide();
    }
}
