﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using static GUI.ViewModel;
using Button = System.Windows.Controls.Button;
using MessageBox = System.Windows.Forms.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace GUI
{
    /// <summary>
    /// AnalyseWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AnalyseWindow : Window
    {
        SettingWindow settingWindow;

        Border previousHighlight;
        [DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(IntPtr hdc, int nIndex);
        public AnalyseWindow()
        {
            InitializeComponent();
            Closed += delegate { settingWindow?.Close(); CheckEditedOnClose();};
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromHwnd(IntPtr.Zero))
            {
                IntPtr hdc = g.GetHdc();
                int vertres = GetDeviceCaps(hdc, 117);
                int horzres = GetDeviceCaps(hdc, 118);
                if (vertres <= Height) Height = vertres - 50;
                if (horzres <= Width) Width = horzres - 50;
            }
            detailBorder.SizeChanged += (sender, e) => detail.Resources["Height"] = detailBorder.ActualHeight - 30;
            detail.Resources["Height"] = detailBorder.ActualHeight -30;

            btn_start.Visibility = paused ? Visibility.Hidden : Visibility.Visible;
            btn_pause.Visibility = started ? Visibility.Visible : Visibility.Hidden;

            btn_start.Content = started ? FindResource("stopImg") : FindResource("startImg");
            btn_pause.Content = paused ? FindResource("startImg") : FindResource("pauseImg");

            statusChanged += StatusChange;
        }

        private void StatusChange(int type)
        {
            if (type == 0)
            {
                btn_start.Visibility = paused ? Visibility.Hidden : Visibility.Visible;
                btn_pause.Content = paused ? FindResource("startImg") : FindResource("pauseImg");
            }
            else if (type == 1)
            {
                btn_start.Content = started ? FindResource("stopImg") : FindResource("startImg");
                btn_pause.Visibility = (btn_pause.Visibility == Visibility.Visible) ? Visibility.Hidden : Visibility.Visible;
            }
        }

        private void Setting_Click(object sender, RoutedEventArgs e)
        {
            settingWindow?.Close();
            LoadSettings();
            settingWindow = new SettingWindow();
            settingWindow.Show();
        }

        private void Btn_start_Click(object sender, RoutedEventArgs e)
        {
            Start();
        }

        private void Btn_pause_Click(object sender, RoutedEventArgs e)
        {
            Pause();
        }
        
        public void ContextBtn_Click(object sender, RoutedEventArgs e)
        {
            DisplayEvents(this, (int)((Button)sender).Tag);
            Border border = ((Border)((Button)sender).Parent);

            if (previousHighlight != null)
                previousHighlight.BorderBrush = Brushes.Transparent;

            border.BorderBrush = Brushes.Red;
            previousHighlight = border;
        }

        public void EventBtn_Click(object sender, RoutedEventArgs e)
        {
            LoadEventDetails(this, (int)((Button)sender).Tag);
            Border border = ((Border)((Button)sender).Parent);

            if (previousHighlight != null)
                previousHighlight.BorderBrush = Brushes.Transparent;

            border.BorderBrush = Brushes.Red;
            previousHighlight = border;
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e) => DeleteSelected(this);

        private void EventImgBtn_Click(object sender, RoutedEventArgs e) => ZoomEvent(this);

        private void SaveBtn_Click(object sender, RoutedEventArgs e) => SaveSequence();

        public void ContextBtn_MouseDoubleClick(object sender, MouseButtonEventArgs e) => ZoomContext(this);

        private void SequenceFileDialogue_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDlg = new OpenFileDialog
            {
                DefaultExt = ".seq",
                Filter = "Sequenz (.seq)|*.seq",
                InitialDirectory = seqPath,
                Multiselect = false
            };


            bool? result = openFileDlg.ShowDialog();

            if (result == true)
            {
                LoadSequence(openFileDlg.FileName);
                DisplayContexts(this);
            }
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollviewer = sender as ScrollViewer;
            if (e.Delta > 0)
                scrollviewer.LineLeft();
            else
                scrollviewer.LineRight();
            e.Handled = true;
        }
    }
}
