﻿using System.Collections.Generic;

namespace GUI
{
    public class SettingsModel
    {
        public bool recordPwd { get; set; }
        public bool blacklist { get; set; }
        public List<string> websiteList { get; set; }
        public int screenshotQuality { get; set; }
        public bool startChrome { get; set; }
    }
}