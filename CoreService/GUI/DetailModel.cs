﻿using System.ComponentModel;

namespace GUI
{
    public class DetailModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string name;
        public string time;
        public string obj;
        public dynamic screenshot;
        public string description;
        public string newContext;
        public string type;
        public string text;
        public string data;
        public string clicktype;
        public string endTime;
        public string scrollXdiff;
        public string scrollYdiff;
        public string range;
        public string dom;

        public int height
        {
            get { return height; }
            set { height = value;  OnPropertyChanged("height");}
        }

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}