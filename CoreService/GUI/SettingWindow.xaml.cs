﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using static GUI.ViewModel;

namespace GUI
{
    /// <summary>
    /// SettingWindow.xaml 的交互逻辑
    /// </summary>
    public partial class SettingWindow : Window
    {
        private string websiteRegex = @"^(https?:\/\/)?(www\.)?([a-zA-Z0-9]+(-?[a-zA-Z0-9])*\.)+[\w]{2,}(\/\S*)?$";

        public SettingWindow()
        {
            InitializeComponent();
            UpdateDisplayedSettings();
        }

        private void UpdateDisplayedSettings()
        {
            CheckBox_Pwd.IsChecked = settings.recordPwd;
            CheckBox_Chrome.IsChecked = settings.startChrome;
            qualitySlider.Value = settings.screenshotQuality;
            rbtn_black.IsChecked = settings.blacklist;
            rbtn_white.IsChecked = !settings.blacklist;
            foreach (string website in settings.websiteList)
                websiteList.Items.Add(website);
        }

        private void Btn_add_OnClick(object sender, RoutedEventArgs e)
        {
            if (websiteTextbox.Text == "") return;
            if (Regex.IsMatch(websiteTextbox.Text, websiteRegex))
            {
                websiteList.Items.Add(websiteTextbox.Text);
                websiteTextbox.Text = "";
            }
            else
                MessageBox.Show("Keine gültige Webadresse.");
        }

        private void SaveBtn_OnClick(object sender, RoutedEventArgs e)
        {
            settings.recordPwd = (bool) CheckBox_Pwd.IsChecked;
            settings.startChrome = (bool) CheckBox_Chrome.IsChecked;
            settings.screenshotQuality = Convert.ToInt32(qualitySlider.Value);
            settings.blacklist = (bool) rbtn_black.IsChecked;

            settings.websiteList.Clear();
            foreach (string website in websiteList.Items) settings.websiteList.Add(website);
            SaveSettings();

            Close();
        }

        private void Btn_delete_OnClick(object sender, RoutedEventArgs e) => websiteList.Items.Remove(websiteList.SelectedItem);

        private void CancelBtn_OnClick(object sender, RoutedEventArgs e) => Close();
    }
}
