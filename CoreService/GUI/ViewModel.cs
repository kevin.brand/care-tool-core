﻿using CoreService.Models;
using CoreService.Models.Events;
using Microsoft.Owin.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using LiquidEngine.Tools;
using Microsoft.Win32;
using Color = System.Drawing.Color;
using Image = System.Windows.Controls.Image;
using Pen = System.Drawing.Pen;

namespace GUI
{
    /// <summary>
    /// Handles the logic behind the GUI
    /// </summary>
    public static class ViewModel
    {
        private const string ChromeAppKey = @"\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe";

        public static string seqPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Documents\Sequenzen";
        public static string settingsPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CaReTrackingTool";
        public static string settingsFilePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CaReTrackingTool\\settings.json";

        static CoreService.SequenceController sequenceController;
        static WindowsRecorder.WindowsRecorder w;

        public delegate void StatusChangedHandler(int type);

        public static event StatusChangedHandler statusChanged;

        private static bool _started;
        private static bool _paused;

        public static bool started
        {
            get => _started;
            set
            {
                _started = value;
                statusChanged?.Invoke(1);
            }
        }

        public static bool paused
        {
            get => _paused;
            set
            {
                _paused = value; 
                statusChanged?.Invoke(0);
            }
        }

        public static Sequence currentSequence;
        public static int currentContext;
        public static int currentEvent;
        public static Selectables selected;
        public static string currentSeqPath;
        static bool edited;

        public static SettingsModel settings;

        public static void Initialize()
        {
            string baseAddress = "http://localhost:15578/";
            WebApp.Start<EventReceiver.Startup>(url: baseAddress);
            sequenceController = new CoreService.SequenceController();
            w = new WindowsRecorder.WindowsRecorder();
            started = false;

            LoadSettings();
            
        }

        public static void Start()
        {
            if (!started)
            {
                sequenceController.Start();
                started = true;

                if (settings.startChrome)
                {
                    string chromeAppFileName = (string)(Registry.GetValue("HKEY_LOCAL_MACHINE" + ChromeAppKey, "", null) ?? Registry.GetValue("HKEY_CURRENT_USER" + ChromeAppKey, "", null));
                    if (!String.IsNullOrEmpty(chromeAppFileName))
                        Process.Start(chromeAppFileName, "https://www.google.de");
                }
            }
            else
            {
                sequenceController.Stop();
                started = false;
            }
        }

        public static void Pause()
        {
            if (started)
            {
                paused = !paused;
                sequenceController.Pause();
            }
        }

        public static void LoadSequence(string filename)
        {
            CheckEditedOnClose();

            string filePath = filename;
            currentSeqPath = filePath;
            string json = File.ReadAllText(filePath);

            Sequence sequence = new Sequence();

            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(sequence.GetType());
            sequence = ser.ReadObject(stream) as Sequence;
            stream.Close();

            currentSequence = sequence;
        }

        public static void CheckEditedOnClose()
        {
            if (edited)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Sie haben ungespeicherte Änderungen. Wollen Sie diese speichern?", "Speichern?", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                    SaveSequence(true);
                edited = false;
            }
        }

        /// <summary>
        /// Loads contexts from current sequence and displays context buttons
        /// </summary>
        /// <param name="window"></param>
        public static void DisplayContexts(AnalyseWindow window)
        {
            window.scrollContext.Children.Clear();
            window.scrollEvent.Children.Clear();
            window.detail.ItemsSource = null;
            if (currentSequence.Equals(null))
                return;

            for (int i = 0; i < currentSequence.numberOfContexts; i++)
            {
                Border highlightBorder = new Border
                {
                    Background = System.Windows.Media.Brushes.Transparent,
                    BorderBrush = System.Windows.Media.Brushes.Transparent,
                    BorderThickness = new Thickness(3)
                };

                Button contextBtn = new Button
                {
                    Tag = i,
                    Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0xFF, 0x1A, 0x1A, 0x1A)),
                    MaxWidth = 380
                };
                contextBtn.Click += window.ContextBtn_Click;
                contextBtn.MouseDoubleClick += window.ContextBtn_MouseDoubleClick;

                StackPanel panel = new StackPanel();

                TextBlock name = new TextBlock
                {
                    Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF)),
                    Text = currentSequence.contexts[i].contextName,
                    HorizontalAlignment = HorizontalAlignment.Center
                };
                panel.Children.Add(name);

                if (currentSequence.contexts[i].screenshot == "NONE")
                {
                    TextBlock txt = new TextBlock
                    {
                        Text = "No Screenshot available",
                        Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF)),
                        TextAlignment = TextAlignment.Center
                    };
                    panel.Children.Add(txt);
                    contextBtn.Content = panel;
                }
                else
                {
                    Image contextImg = new Image();

                    BitmapImage img = new BitmapImage();
                    string imgBase64 = currentSequence.contexts[i].screenshot;
                    byte[] bytes = Convert.FromBase64String(imgBase64);
                    MemoryStream stream = new MemoryStream();
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Seek(0, SeekOrigin.Begin);
                    img.BeginInit();
                    img.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                    img.CacheOption = BitmapCacheOption.OnLoad;
                    img.StreamSource = stream;
                    img.EndInit();
                    stream.Close();

                    contextImg.Source = img;

                    panel.Children.Add(contextImg);
                    contextBtn.Content = panel;
                }

                highlightBorder.Child = contextBtn;
                window.scrollContext.Children.Add(highlightBorder);
            }
        }

        /// <summary>
        /// Loads events from current context and displays event buttons
        /// </summary>
        /// <param name="window"></param>
        /// <param name="contextInd"></param>
        public static void DisplayEvents(AnalyseWindow window, int contextInd)
        {
            currentContext = contextInd;
            selected = Selectables.Context;
            window.scrollEvent.Children.Clear();
            window.detail.ItemsSource = null;
            for (int i = 0; i < currentSequence.contexts[contextInd].numberOfEvents; i++)
            {
                Border highlightBorder = new Border
                {
                    Background = System.Windows.Media.Brushes.Transparent,
                    BorderBrush = System.Windows.Media.Brushes.Transparent,
                    BorderThickness = new Thickness(3)
                };

                Button eventBtn = new Button
                {
                    Tag = i, Background = (SolidColorBrush) (new BrushConverter().ConvertFrom("#FF202225"))
                };
                eventBtn.Click += window.EventBtn_Click;

                highlightBorder.Child = eventBtn;

                StackPanel pnl = new StackPanel();

                TextBlock txt1 = new TextBlock
                {
                    Foreground = System.Windows.Media.Brushes.White,
                    Text = currentSequence.contexts[contextInd].events[i].name
                };

                TextBlock txt2 = new TextBlock
                {
                    Foreground = System.Windows.Media.Brushes.White,
                    Text = currentSequence.contexts[contextInd].events[i].time
                };

                pnl.Children.Add(txt1);
                pnl.Children.Add(txt2);

                eventBtn.Content = pnl;

                window.scrollEvent.Children.Add(highlightBorder);
            }
        }
        
        /// <summary>
        /// Loads details from current event and displays them
        /// </summary>
        /// <param name="window"></param>
        /// <param name="eventId"></param>
        public static void LoadEventDetails(AnalyseWindow window, int eventId)
        {
            currentEvent = eventId;
            selected = Selectables.Event;

            EventModel tempEvent = currentSequence.contexts[currentContext].events[currentEvent];

            Image eventImg = new Image();

            if (tempEvent.screenshot != "NONE")
            {
                BitmapImage img = new BitmapImage();
                string imgBase64 = tempEvent.screenshot;
                byte[] bytes = Convert.FromBase64String(imgBase64);
                MemoryStream stream = new MemoryStream();
                stream.Write(bytes, 0, bytes.Length);
                stream.Seek(0, SeekOrigin.Begin);
                MemoryStream stream2 = new MemoryStream();

                if (tempEvent.GetType() == new MouseEvent().GetType())
                {
                    System.Drawing.Image bm = Bitmap.FromStream(stream);
                    using (Graphics graphics = Graphics.FromImage(bm))
                    {
                        Rectangle rect = new Rectangle(((MouseEvent)tempEvent).xCoordinate - 5 >= 0 ? ((MouseEvent)tempEvent).xCoordinate - 5 : ((MouseEvent)tempEvent).xCoordinate, ((MouseEvent)tempEvent).yCoordinate - 5 >= 0 ? ((MouseEvent)tempEvent).yCoordinate - 5 : ((MouseEvent)tempEvent).yCoordinate, 10, 10);
                        using (Pen pen = new Pen(Color.Red, 3))
                        {
                            graphics.DrawEllipse(pen, rect);
                        }
                    }
                    bm.Save(stream2, System.Drawing.Imaging.ImageFormat.Bmp);
                    stream2.Seek(0, SeekOrigin.Begin);
                }

                img.BeginInit();
                img.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.StreamSource = tempEvent.GetType() == new MouseEvent().GetType() ? stream2 : stream;
                img.EndInit();

                stream2.Close();
                stream.Close();
                eventImg.Source = img;

                if (tempEvent.GetType() == new CollectionEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((CollectionEvent)tempEvent).name, time = "Zeitpunkt: " + ((CollectionEvent)tempEvent).time, ((CollectionEvent)tempEvent).obj, screenshot = eventImg } };
                else if (tempEvent.GetType() == new ContextSwitch().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((ContextSwitch)tempEvent).name, time = "Zeitpunkt: " + ((ContextSwitch)tempEvent).time, ((ContextSwitch)tempEvent).description, newContext = "Neuer Kontext: " + ((ContextSwitch)tempEvent).newContext, screenshot = eventImg } };
                else if (tempEvent.GetType() == new CopyCutPasteEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((CopyCutPasteEvent)tempEvent).name, time = "Zeitpunkt: " + ((CopyCutPasteEvent)tempEvent).time, type = "Typ: " + ((CopyCutPasteEvent)tempEvent).type, text  = "" + ((CopyCutPasteEvent)tempEvent).text, screenshot = eventImg } };
                else if (tempEvent.GetType() == new FindEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((FindEvent)tempEvent).name, time = "Zeitpunkt: " + ((FindEvent)tempEvent).time, screenshot = eventImg } };
                else if (tempEvent.GetType() == new FormSubmitEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((FormSubmitEvent)tempEvent).name, time = "Zeitpunkt: " + ((FormSubmitEvent)tempEvent).time, data = "Daten" + ((FormSubmitEvent)tempEvent).data, screenshot = eventImg } };
                else if (tempEvent.GetType() == new MouseEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((MouseEvent)tempEvent).name, time = "Zeitpunkt: " + ((MouseEvent)tempEvent).time, ((MouseEvent)tempEvent).clicktype, xCoordinate = "X: " + ((MouseEvent)tempEvent).xCoordinate, yCoordinate = "Y: " + ((MouseEvent)tempEvent).yCoordinate, screenshot = eventImg } };
                else if (tempEvent.GetType() == new PauseEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((PauseEvent)tempEvent).name, time = "Startzeit: " + ((PauseEvent)tempEvent).time, endTime = "Endzeit: " + ((PauseEvent)tempEvent).endTime, screenshot = "Kein Screenshot vorhanden." } };
                else if (tempEvent.GetType() == new ScrollEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((ScrollEvent)tempEvent).name, time = "Zeitpunkt: " + ((ScrollEvent)tempEvent).time, scrollXdiff = "X Delta: " + (((ScrollEvent)tempEvent).xStartCoordinate - ((ScrollEvent)tempEvent).xEndCoordinate), scrollYdiff = "Y Delta: " + (((ScrollEvent)tempEvent).yStartCoordinate - ((ScrollEvent)tempEvent).yEndCoordinate), screenshot = eventImg } };
                else if (tempEvent.GetType() == new SelectEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((SelectEvent)tempEvent).name, time = "Zeitpunkt: " + ((SelectEvent)tempEvent).time, data = "Ausgewähltes Objekt: " + ((SelectEvent)tempEvent).data, screenshot = eventImg } };
                else if (tempEvent.GetType() == new SortEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((SortEvent)tempEvent).name, time = "Zeitpunkt: " + ((SortEvent)tempEvent).time, range = "Bereich: " + ((SortEvent)tempEvent).range, type = "Art: " + ((SortEvent)tempEvent).type, screenshot = eventImg } };
                else if (tempEvent.GetType() == new TextInputEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((TextInputEvent)tempEvent).name, time = "Zeitpunkt: " + ((TextInputEvent)tempEvent).time, text = "Input: " + ((TextInputEvent)tempEvent).text, dom = "Objekt: " + ((TextInputEvent)tempEvent).dom, screenshot = eventImg } };
                else if (tempEvent.GetType() == new TextSelectEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((TextSelectEvent)tempEvent).name, time = "Zeitpunkt: " + ((TextSelectEvent)tempEvent).time, text = ((TextSelectEvent)tempEvent).text.Replace('屌', '\n'), screenshot = eventImg } };
            }
            else
            {
                string screenshotText = "Kein Screenshot vorhanden.";

                if (tempEvent.GetType() == new CollectionEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((CollectionEvent)tempEvent).name, time = "Zeitpunkt: " + ((CollectionEvent)tempEvent).time, ((CollectionEvent)tempEvent).obj, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new ContextSwitch().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((ContextSwitch)tempEvent).name, time = "Zeitpunkt: " + ((ContextSwitch)tempEvent).time, ((ContextSwitch)tempEvent).description, newContext = "Neuer Kontext: " + ((ContextSwitch)tempEvent).newContext, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new CopyCutPasteEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((CopyCutPasteEvent)tempEvent).name, time = "Zeitpunkt: " + ((CopyCutPasteEvent)tempEvent).time, type = "Typ: " + ((CopyCutPasteEvent)tempEvent).type, text = "" + ((CopyCutPasteEvent)tempEvent).text, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new FindEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((FindEvent)tempEvent).name, time = "Zeitpunkt: " + ((FindEvent)tempEvent).time, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new FormSubmitEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((FormSubmitEvent)tempEvent).name, time = "Zeitpunkt: " + ((FormSubmitEvent)tempEvent).time, data = "Daten" + ((FormSubmitEvent)tempEvent).data, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new MouseEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((MouseEvent)tempEvent).name, time = "Zeitpunkt: " + ((MouseEvent)tempEvent).time, ((MouseEvent)tempEvent).clicktype, xCoordinate = "X: " + ((MouseEvent)tempEvent).xCoordinate, yCoordinate = "Y: " + ((MouseEvent)tempEvent).yCoordinate, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new PauseEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((PauseEvent)tempEvent).name, time = "Startzeit: " + ((PauseEvent)tempEvent).time, endTime = "Endzeit: " + ((PauseEvent)tempEvent).endTime, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new ScrollEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((ScrollEvent)tempEvent).name, time = "Zeitpunkt: " + ((ScrollEvent)tempEvent).time, scrollXdiff = "X Delta: " + (((ScrollEvent)tempEvent).xStartCoordinate - ((ScrollEvent)tempEvent).xEndCoordinate), scrollYdiff = "Y Delta: " + (((ScrollEvent)tempEvent).yStartCoordinate - ((ScrollEvent)tempEvent).yEndCoordinate), screenshot = screenshotText } };
                else if (tempEvent.GetType() == new SelectEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((SelectEvent)tempEvent).name, time = "Zeitpunkt: " + ((SelectEvent)tempEvent).time, data = "Ausgewähltes Objekt: " + ((SelectEvent)tempEvent).data, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new SortEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((SortEvent)tempEvent).name, time = "Zeitpunkt: " + ((SortEvent)tempEvent).time, range = "Bereich: " + ((SortEvent)tempEvent).range, type = "Art: " + ((SortEvent)tempEvent).type, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new TextInputEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((TextInputEvent)tempEvent).name, time = "Zeitpunkt: " + ((TextInputEvent)tempEvent).time, text = "Input: " + ((TextInputEvent)tempEvent).text, dom = "Objekt: " + ((TextInputEvent)tempEvent).dom, screenshot = screenshotText } };
                else if (tempEvent.GetType() == new TextSelectEvent().GetType())
                    window.detail.ItemsSource = new List<object> { new { ((TextSelectEvent)tempEvent).name, time = "Zeitpunkt: " + ((TextSelectEvent)tempEvent).time, text = ((TextSelectEvent)tempEvent).text.Replace('屌', '\n'), screenshot = screenshotText } };
            }
        }

        public static void DeleteSelected(AnalyseWindow window)
        {
            if (selected == Selectables.Event)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Ausgewähltes " + currentSequence.contexts[currentContext].events[currentEvent].name + " löschen?", "Löschen bestätigen", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    currentSequence.contexts[currentContext].events.RemoveAt(currentEvent);
                    currentSequence.contexts[currentContext].numberOfEvents--;
                    DisplayEvents(window, currentContext);
                    edited = true;
                }
            }
            else if (selected == Selectables.Context)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Ausgewählten Kontext " + currentSequence.contexts[currentContext].contextName + " löschen?", "Löschen bestätigen", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    currentSequence.contexts.RemoveAt(currentContext);
                    currentSequence.numberOfContexts--;
                    DisplayContexts(window);
                    edited = true;
                }
            }
        }

        public static void SaveSequence(bool fromClosing = false)
        {
            if (currentSeqPath == null) return;

            MessageBoxResult messageBoxResult = MessageBoxResult.No;

            if (!fromClosing)
                messageBoxResult = MessageBox.Show("Änderungen an geladener Sequenz speichern?", "Speichern bestätigen", MessageBoxButton.YesNo);

            if (messageBoxResult == MessageBoxResult.Yes || fromClosing)
            {
                if (File.Exists(currentSeqPath))
                    File.Delete(currentSeqPath);

                MemoryTributary stream = new MemoryTributary();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Sequence));
                ser.WriteObject(stream, currentSequence);
                stream.Position = 0;
                StreamReader sr = new StreamReader(stream);

                if (!Directory.Exists(seqPath))
                    Directory.CreateDirectory(seqPath);

                File.WriteAllText(currentSeqPath, sr.ReadToEnd());
                stream.Close();

                MessageBox.Show("Sequenz gespeichert!");
            }
        }

        public static void ZoomEvent(AnalyseWindow window)
        {
            EventModel tempEvent = currentSequence.contexts[currentContext].events[currentEvent];

            if (tempEvent.screenshot == "NONE")
                return;

            Image eventImg = new Image();
            System.Drawing.Image bm;
            BitmapImage img = new BitmapImage();
            string imgBase64 = tempEvent.screenshot;
            byte[] bytes = Convert.FromBase64String(imgBase64);
            MemoryStream stream = new MemoryStream();
            stream.Write(bytes, 0, bytes.Length);
            stream.Seek(0, SeekOrigin.Begin);
            MemoryStream stream2 = new MemoryStream();

            if (tempEvent.GetType() == new MouseEvent().GetType())
            {
                bm = Bitmap.FromStream(stream);
                using (Graphics graphics = Graphics.FromImage(bm))
                {
                    Rectangle rect = new Rectangle(((MouseEvent)tempEvent).xCoordinate - 5 >= 0 ? ((MouseEvent)tempEvent).xCoordinate - 5 : ((MouseEvent)tempEvent).xCoordinate, ((MouseEvent)tempEvent).yCoordinate - 5 >= 0 ? ((MouseEvent)tempEvent).yCoordinate - 5 : ((MouseEvent)tempEvent).yCoordinate, 10, 10);
                    using (Pen pen = new Pen(Color.Red, 3))
                    {
                        graphics.DrawEllipse(pen, rect);
                    }
                }
                bm.Save(stream2, System.Drawing.Imaging.ImageFormat.Bmp);
                stream2.Seek(0, SeekOrigin.Begin);

            }
            img.BeginInit();
            img.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = tempEvent.GetType() == new MouseEvent().GetType() ? stream2 : stream;

            img.EndInit();

            stream2.Close();
            stream.Close();
            eventImg.Source = img;

            EventZoom zoom = new EventZoom
            {
                Height = eventImg.Height,
                Width = eventImg.Width
            };

            window.Closed += (sender, args) => zoom.Close();

            zoom.screenshot.Content = eventImg;
            zoom.Show();
        }

        public static void ZoomContext(AnalyseWindow window)
        {
            if (currentSequence.contexts[currentContext].screenshot == "NONE")
                return;

            Image contextImage = new Image();
            BitmapImage img = new BitmapImage();
            string imgBase64 = currentSequence.contexts[currentContext].screenshot;
            byte[] bytes = Convert.FromBase64String(imgBase64);
            MemoryStream stream = new MemoryStream();
            stream.Write(bytes, 0, bytes.Length);
            stream.Seek(0, SeekOrigin.Begin);
            img.BeginInit();
            img.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = stream;
            img.EndInit();
            stream.Close();
            contextImage.Source = img;

            EventZoom zoom = new EventZoom
            {
                Height = contextImage.Height,
                Width = contextImage.Width
            };

            window.Closed += (sender, args) => zoom.Close();

            zoom.screenshot.Content = contextImage;
            zoom.Show();
        }

        public static void LoadSettings()
        {

            if (!Directory.Exists(settingsPath))
                Directory.CreateDirectory(settingsPath);

            if (!File.Exists(settingsFilePath))
            {

                settings = new SettingsModel
                {
                    blacklist = true,
                    websiteList = new List<string>(),
                    screenshotQuality = 50,
                    startChrome = true
                };
                SaveSettings();
            }
            else
            {
                string json = File.ReadAllText(settingsFilePath);
                settings = JsonConvert.DeserializeObject<SettingsModel>(json);
            }
        }

        public static void SaveSettings()
        {
            if (!Directory.Exists(settingsPath))
                Directory.CreateDirectory(settingsPath);

            string json = JsonConvert.SerializeObject(settings);
            if (File.Exists(settingsFilePath))
                File.Delete(settingsFilePath);
            File.WriteAllText(settingsFilePath, json);
        }
    }

    public enum Selectables
    {
        Event, Context
    }
}
